# bootmap

A foolproof system restore tool, EFI image manager and snapshot manager for UEFI systems.

## Description

Never hose your Arch Linux system again! Bootmap always keeps the last N working copies of your system, allowing you to recover from virtually any misconfiguration without the need to carry a rescue disk around all the time.

Bootmap is borne out of the author's past record of [Arch Linux screwups](https://www.reddit.com/r/ProgrammerHumor/comments/6gsoto/archlinux_fuckup_assessment_form) resulting in a non-booting or otherwise broken system. It is designed to allow the system to boot even after

 * Accidental `rm -rf / --no-preserve-root`.
 * Deleting your bootloader.

If you're thinking this sounds a lot like [snapper](http://snapper.io), you're right! The difference is that on UEFI systems, the images on the EFI partition need to be kept in sync with the backup snapshots snapper makes, so a new, integrated tool is required.

### Example

Here's a screenshot of bootmap running on a typical system:

![Output of `bootmap status`, showing linux and linux-zen on the current system and linux-zen on the backup.](pages/screenshot.png)

You can see that bootmap made a backup snapshot of the system the last time `pacman -Syu` was run, and kept a backup EFI image of an old kernel to make it bootable. The system itself has two EFI images that boot into it, corresponding to the two kernels the user has chosen to install.

This also solves a common complaint among Arch users: you can't keep old kernels around. (See [here](https://bugs.archlinux.org/task/16702), or [here](https://unix.stackexchange.com/questions/50579).) Bootmap not only keeps old kernels around, but old systems too, providing even more robustness.

### Features

Bootmap has all the features of a typical EFI image generator like [sbupdate](https://github.com/andreyv/sbupdate/blob/master/sbupdate) or as of version 31, [mkinitcpio](https://wiki.archlinux.org/title/Mkinitcpio), including

 * Generates, installs and optionally signs EFI images with your own Secure Boot keys.
 * Automatically pulls in microcode, splash, and custom kernel commandline into the images.
 * Handles kernel updates/installation/removal automatically.

In addition, it also

 * Keeps a set number of backup images/snaphots, and ensures they are kept in sync and bootable.
 * Makes snapshots only when necessary. For example, multiple `pacman -Syu`s do not trigger multiple snapshots.
 * Has many layers of safety, to prevent breakage by either user mishaps or bugs.
 * Allows full extensibility through scripting, to support set-ups I haven't imagined.
 * Has a super handy `status` command!

### Disclaimer

Bootmap does not attempt to protect you from filesystem damage. If you type the wrong device when you're `dd`ing a usb drive, good luck recovering that (it's been done). For this situation, you should have off-site backups. (Though *should* does not mean *yes*.)

### Name

Bootmap gets its (stupid) name from the fact that it uses a directory of symlinks to point each EFI image to the right snapshot to boot into. This is the only possible design that doesn't involve rewriting EFI images after they are first generated, which would introduce a possible failure mechanism where all the EFI images get wiped out at the same time if the tools used to do the rewriting are silently broken.

## Further reading

 * For installation instructions, configuration, and more information about bootmap's operation, see [Installation](pages/Installation.md).

 * For the manual, see [bootmap.8](https://glcdn.githack.com/pbs3141/bootmap/-/raw/master/pages/bootmap.8.html).
 
 * For an example of a full system installation utilising bootmap, see [Example Installation](https://wiki.archlinux.org/index.php?title=User:PBS/Example_installation_with_bootmap).
