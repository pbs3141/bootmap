#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <vector>
#include <map>
#include <memory>
#include <algorithm>
#include <optional>
#include <ranges>
#include <tuple>
#include <array>
#include <random>
#include <regex>
#include <limits>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
namespace fs = std::filesystem;

extern "C" {
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <libmount.h>
#include <pipeline.h>
#include <ini.h>
}

const char *help_msg =
    "Usage: bootmap COMMAND\n"
    "  status   Print current images and subvolumes\n"
    "  install  Install missing images\n"
    "  cleanup  Remove extraneous files\n"
    "  pre      Hook to rotate backups and remove images\n"
    "  post     Hook to create images\n"
    "  help     Show this help message\n";

const char *RED = "\033[31m";
const char *GRN = "\033[32m";
const char *YLW = "\033[33m";
const char *NRM = "\033[39m";
const char *UN1 = "\033[4m";
const char *UN0 = "\033[24m";

struct _warn {friend std::ostream& operator<<(std::ostream &os, _warn) {return os << YLW << "Warning: " << NRM;}};
#define WARN _warn()

struct _err {friend std::ostream& operator<<(std::ostream &os, _err) {return os << RED << "Error: " << NRM;}};
#define ERR _err()

/*
 * Basic utilities
 */

auto trim_view(std::string_view view)
{
    while (!view.empty() && std::isspace(view.front())) view.remove_prefix(1);
    while (!view.empty() && std::isspace(view.back ())) view.remove_suffix(1);
    return view;
}

auto trim(const std::string &str)
{
    return std::string(trim_view(std::string_view(str)));
}

auto remnant(std::stringstream &ss, const std::string &str)
{
    auto pos = ss.tellg();
    if (pos == -1) pos = str.length();
    return std::string_view(str).substr(pos);
}

template <typename T, typename F>
void delete_if(T &t, const F &f)
{
    for (auto it = std::begin(t); it != std::end(t); )
        if (f(*it))
        {
            *it = std::move(t.back());
            t.pop_back();
        }
        else
            ++it;
}

template <typename T>
std::string comma_separated(T strings)
{
    std::string result;
    
    for (auto str : strings)
    {
        if (&str != &strings.front()) result += ", ";
        result += str;
    }
    
    return result;
}

/*
 * Running external commands
 */

struct to_cstr
{
    const char *s;
    
    explicit to_cstr(const char *s) : s(s) {}
    explicit to_cstr(const std::string &str) : s(str.c_str()) {}
    explicit to_cstr(const fs::path &path) : s(path.c_str()) {}
    
    operator const char*() const {return s;}
};

template<typename... Args>
int run_command(Args&&... args)
{
    std::cout << std::flush;
    auto p = pipeline_new();
    pipeline_command_args(p, static_cast<to_cstr>(args)..., NULL);
    return pipeline_run(p);
}

template<typename... Args>
auto command_output(Args&&... args)
{
    std::string result;
    
    auto p = pipeline_new();
    pipeline_command_args(p, static_cast<to_cstr>(args)..., NULL);
    pipeline_want_out(p, -1);
    pipeline_start(p);
    while (auto line = pipeline_readline(p)) result += line;
    pipeline_free(p);
    
    return result;
}

/*
 * Basic system utilities
 */

auto absolute(fs::path path, const fs::path &base)
{
    return path.is_absolute() ? std::move(path) : base / path;
}

bool is_btrfs_subvolume(const char *path)
{
    int fd = open(path, O_RDONLY);
    if (fd < 0) return false;
    
    struct stat st;
    int ret = fstat(fd, &st);
    close(fd);
    
    return ret >= 0 && st.st_ino == 256;
}

bool is_btrfs_subvolume(const fs::path &path)
{
    return is_btrfs_subvolume(path.c_str());
}

void delete_btrfs_subvolume(const::fs::path &path)
{
    if (fs::canonical(path) == "/") throw std::runtime_error("Sanity failure (tried to delete the system root subvolume)");
    run_command("btrfs", "subvolume", "delete", path);
}

void ensure_automounted(const std::string &path)
{
    close(open(path.c_str(), O_RDONLY));
}

void ensure_root()
{
    if (geteuid() != 0)
        throw std::runtime_error("You need to be root to run this command");
}

/*
 * Table pretty-printer
 */

template<typename V>
void multi_emplace(std::vector<V>&) {}

template<typename V, typename T, typename... Ts>
void multi_emplace(std::vector<V> &v, T &&t, Ts&&... ts)
{
    v.emplace_back(std::move(t));
    multi_emplace(v, std::forward<Ts>(ts)...);
}

auto length_in_terminal(const std::string &str)
{
    int result = 0;
    
    for (int i = 0; i < str.length(); i++)
    {
        if ((str[i] & 0b11000000) == 0b10000000) continue;
        
        if (str[i] == '\033' && i + 1 < str.length() && str[i + 1] == '[')
        {
            i += 2;
            while (i < str.length() && str[i] != 'm') i++;
            continue;
        }
        
        result++;
    }
    
    return result;
}

class Table
{
    std::vector<std::vector<std::string>> rows;
    
public:
    bool header = true;
    int margin = 0;
    int separator = 2;
    
    template<typename... Ts>
    void add_row(Ts&&... ts)
    {
        rows.emplace_back();
        multi_emplace(rows.back(), std::forward<Ts>(ts)...);
    }
    
    void print(std::ostream &os) const
    {
        if (rows.empty()) return;
        
        std::vector<int> widths(rows.front().size(), 0);
        for (auto &r : rows)
            for (int i = 0; i < r.size(); i++)
                widths[i] = std::max(widths[i], length_in_terminal(r[i]));
        
        auto space = [&] (int count) {for (int i = 0; i < count; i++) os << ' ';};
        
        for (auto &r : rows)
        {
            if (header && &r == &rows.front()) os << UN1;
            space(margin);
            for (int i = 0; i < r.size(); i++) {if (i != 0) space(separator); os << r[i]; space(widths[i] - length_in_terminal(r[i]));}
            space(margin);
            os << '\n';
            if (header && &r == &rows.front()) os << UN0;
        }
    }
};

/*
 * Collision-free multiple renamer
 */

class BatchRenamer
{
    std::vector<std::pair<std::string, std::string>> pairs;
    int start;
    
    void process(int i)
    {
        for (int j = 0; j < pairs.size(); j++)
            if (pairs[i].second == pairs[j].first)
            {
                if (j == start)
                {
                    std::string tmp;
                    for (int x = 0; fs::exists(tmp = pairs[j].first + ".tmp" + std::to_string(x)); x++) {}
                    fs::rename(pairs[j].first, tmp);
                    pairs[j].first = std::move(tmp);
                }
                else
                {
                    process(j);
                }
                
                break;
            }
        
        fs::rename(pairs[i].first, pairs[i].second);
        pairs[i].first.clear();
    }
    
public:
    void rename(std::string &src, std::string dst)
    {
        if (src != dst) pairs.emplace_back(std::move(src), dst);
        src = std::move(dst);
    }
    
    void commit()
    {
        for (start = 0; start < pairs.size(); start++)
            if (!pairs[start].first.empty())
                process(start);
        
        pairs.clear();
    }
};

/*
 * Filesystem temporary operations
 */

struct TmpDir
{
    std::string path;
    TmpDir(std::string path_) : path(std::move(path_)) {try {fs::create_directory(path);} catch (const fs::filesystem_error&) {throw std::runtime_error("Failed to create temporary directory " + path);}}
    ~TmpDir() {try {fs::remove(path);} catch (const fs::filesystem_error&) {std::cerr << WARN << "Failed to remove " << path << " - Please delete manually.\n";}}
};

struct TmpMount
{
    std::string path;
    bool internal;
    struct mount_failure : std::runtime_error {mount_failure() : std::runtime_error("Error in mount()") {}};
    TmpMount(const std::string &device, const std::string &fstype, unsigned long flags, const std::string &options, std::string path_) : path(std::move(path_)), internal(true) {if (mount(device.c_str(), path.c_str(), fstype.c_str(), flags, options.c_str()) < 0) throw mount_failure();}
    TmpMount(const std::string &device, const std::string &subvol, std::string path_) try : TmpMount(device, "btrfs", MS_NOATIME, "subvol=" + subvol, std::move(path_)) {} catch (const mount_failure&) {throw std::runtime_error("Failed to mount subvolume " + subvol + " of " + device + " at " + path);}
    TmpMount(const std::string &path_) : path(std::move(path_)), internal(false) {if (run_command("mount", path) != 0) throw std::runtime_error("Failed to mount " + path);}
    ~TmpMount() {internal ? umount(path.c_str()) : run_command("umount", path);}
};

struct TmpRemountRW
{
    std::string path;
    bool bind;
    TmpRemountRW(std::string path_, bool bind) : path(std::move(path_)), bind(bind) {if (mount(nullptr, path.c_str(), nullptr, MS_REMOUNT | (bind ? MS_BIND : 0), "") < 0) throw std::runtime_error("Failed to remount read-write " + path);}
    ~TmpRemountRW() {mount(nullptr, path.c_str(), nullptr, MS_REMOUNT | (bind ? MS_BIND : 0) | MS_RDONLY, "");}
};

/*
 * Libmount wrapper
 */

template <typename Tp, typename Tf>
auto to_unique_ptr(Tp *p, Tf f)
{
    return std::unique_ptr<Tp, Tf>(p, f);
}

auto get_libmount_table(const char *path)
{
    auto tb = mnt_new_table_from_file(path);
    if (!tb) throw std::runtime_error("Failed to read " + std::string(path));
    return to_unique_ptr(tb, mnt_free_table);
}

auto get_mountinfo()
{
    return get_libmount_table("/proc/self/mountinfo");
}

auto get_fstab()
{
    return get_libmount_table("/etc/fstab");
}

auto get_root_mount(struct libmnt_table *tb)
{
    struct libmnt_fs *fs;
    mnt_table_get_root_fs(tb, &fs);
    if (!fs) throw std::runtime_error("Failed to find mount info for system root");
    return fs;
}

/*
 * Commands
 */

enum class Command
{
    Status,
    Install,
    Cleanup,
    Pre,
    Post,
    Help
};

Command get_command(int argc, char **argv)
{
    if (argc == 1) return Command::Status;
    if (argc > 2) throw std::runtime_error("Too many arguments");
    
         if (std::strcmp(argv[1], "status" ) == 0) return Command::Status;
    else if (std::strcmp(argv[1], "install") == 0) return Command::Install;
    else if (std::strcmp(argv[1], "cleanup") == 0) return Command::Cleanup;
    else if (std::strcmp(argv[1], "pre"    ) == 0) return Command::Pre;
    else if (std::strcmp(argv[1], "post"   ) == 0) return Command::Post;
    else if (std::strcmp(argv[1], "help"   ) == 0) return Command::Help;
    else if (std::strcmp(argv[1], "--help" ) == 0) return Command::Help;
    else if (std::strcmp(argv[1], "-h"     ) == 0) return Command::Help;
    
    throw std::runtime_error("Unrecognised command " + std::string(argv[1]));
}

/*
 * Config file
 */

std::string discover_esp()
{
         if (fs::exists("/efi"     )) return "/efi";
    else if (fs::exists("/boot/efi")) return "/boot/efi";
    else if (fs::exists("/boot"    )) return "/boot";
    else                              throw std::runtime_error("Failed to auto-discover EFI system partition");
}

enum class ImageNameFormat
{
    Version,
    Date,
    Random
};

struct Config
{
    // Static
    std::string esp = discover_esp();
    std::string bootmapdir = "/run/bootmap";
    
    // Read from file
    std::string bootmapvol = "/bootmap";
    std::string imagedir = "EFI/Linux";
    std::string rootvol = "/@";
    int maxbackups = 1;
    ImageNameFormat nameformat = ImageNameFormat::Date;
    std::string bootnext = "linux";
};

int load_config_callback(void *user, const char*, const char *name, const char *value)
{
    auto &config = *static_cast<Config*>(user);
    
         if (std::strcmp(name, "BootmapVol") == 0) config.bootmapvol = value;
    else if (std::strcmp(name, "ImageDir"  ) == 0) config.imagedir   = value;
    else if (std::strcmp(name, "RootVol"   ) == 0) config.rootvol    = value;
    else if (std::strcmp(name, "MaxBackups") == 0) {long ret; errno = 0; ret = std::strtol(value, nullptr, 10); if (errno || ret > std::numeric_limits<int>::max()) std::cerr << WARN << "Ignoring invalid value " << value << " for MaxBackups\n"; else config.maxbackups = ret;}
    else if (std::strcmp(name, "NameFormat") == 0) {if (std::strcmp(value, "date") == 0) config.nameformat = ImageNameFormat::Date; else if (std::strcmp(value, "version") == 0) config.nameformat = ImageNameFormat::Version; else if (std::strcmp(value, "random") == 0) config.nameformat = ImageNameFormat::Random; else std::cerr << WARN << "Ignoring invalid value " << value << " for NameFormat\n";}
    else if (std::strcmp(name, "BootNext"  ) == 0) config.bootnext   = value;
    else                                           std::cerr << WARN << "Ignoring unknown config variable " << name << '\n';
    
    return 1;
}

Config load_config()
{
    Config config;
    
    if (ini_parse("/etc/bootmap.conf", load_config_callback, static_cast<void*>(&config)) < 0)
        throw std::runtime_error("Failed to read config file");
    
    if (config.maxbackups < 0)
        config.maxbackups = std::numeric_limits<int>::max();
    
    if (config.bootmapvol.empty()) throw std::runtime_error("BootmapVol is empty");
    if (config.imagedir.empty()  ) throw std::runtime_error("ImageDir is empty");
    if (config.rootvol.empty()   ) throw std::runtime_error("RootVol is empty");
    
    return config;
}

// The global config variable, which lives on main()'s stack
const Config *config;

/*
 * Image name detection
 */

bool is_uniq_suffix(std::string_view view)
{
    return view.empty() || std::regex_match(view.begin(), view.end(), std::regex("-\\d+"));
}

bool is_version_suffix(const std::string &str)
{
    std::stringstream ss(str);
    
    auto consume_ch = [&] (char ch_) {char ch; ss >> ch; return ss && ch == ch_;};
    auto consume_int = [&] () {int x; ss >> x; return (bool)ss;};
    
    if (!consume_ch('-')) return false;
    
    for (int i = 0; ; i++)
    {
        if (!consume_int()) return false;
        if (i == 2) break;
        if (!consume_ch('.')) return false;
    }
    
    return is_uniq_suffix(remnant(ss, str));
}

bool is_date_suffix(const std::string &str)
{
    std::stringstream ss(str);
    
    std::tm tm;
    ss >> std::get_time(&tm, "-%Y-%m-%d-%H-%M");
    if (!ss) return false;
    
    return is_uniq_suffix(remnant(ss, str));
}

bool is_random_suffix(const std::string &str)
{
    return std::regex_match(str, std::regex("-\\d{8}"));
}

bool is_image_suffix(const std::string &str)
{
    return is_date_suffix(str) || is_version_suffix(str) || is_random_suffix(str);
}

/*
 * Main structures
 */

struct Link
{
    Link(std::string name, std::string target) : name(std::move(name)), target(std::move(target)) {}
    Link(const fs::path &path) : Link(path.filename().string(), absolute(fs::read_symlink(path), config->bootmapvol).lexically_normal()) {}
    
    // Set by constructor
    std::string name;
    std::string target;
    
    static std::string imagename(const std::string &name) {return name + ".efi";}
    static std::string imagepath(const std::string &name) {return config->esp + "/" + config->imagedir + "/" + imagename(name);}
    static std::string path     (const std::string &name) {return config->bootmapdir + "/" + name;}
    static std::string bootpath (const std::string &name) {return config->bootmapvol + "/" + name;}
    std::string imagename() const {return imagename(name);}
    std::string imagepath() const {return imagepath(name);}
    std::string path     () const {return path(name);}
    std::string bootpath () const {return bootpath(name);}
    
    bool matches_kernel(const std::string &kernel) const {return name.starts_with(kernel) && is_image_suffix(name.substr(kernel.length()));}
    
    // Create the symlink
    void create_link()
    {
        fs::create_symlink(target, path());
    }
    
    // Update the symlink destination
    void update_target(std::string target_)
    {
        target = std::move(target_);
        fs::remove(path());
        create_link();
    }
    
    // Remove both the symlink and image
    void remove_all() const
    {
        fs::remove(path());
        fs::remove(imagepath());
    }
};

struct Subvol
{
    Subvol(std::string path, std::string bootpath) : path(std::move(path)), bootpath(std::move(bootpath)) {}
    Subvol(const std::string &path) : Subvol(path, config->bootmapvol / fs::path(path).filename()) {}
    
    // Set by constructor
    std::string path, bootpath;
    
    // List of images pointing to this Subvol
    std::vector<std::unique_ptr<Link>> images;
    
    // Backup number (0 for root subvolume, 1,2,3,... for backups, NonBackup for other)
    static constexpr auto NonBackup = std::numeric_limits<int>::max();
    int num;
    
    // Remove the subvolume along with all its images
    void remove_all() const
    {
        delete_btrfs_subvolume(path);
        for (auto &im : images) im->remove_all();
    }
};

/*
 * Image name creation
 */

using VersionMap = std::map<std::string, std::tuple<int, int, int>>;

auto load_versionmap()
{
    VersionMap result;
    
    for (const auto &d : fs::directory_iterator("/usr/lib/modules"))
    {
        auto f = std::ifstream(d.path() / "pkgbase");
        if (!f) continue;
        
        std::string line;
        if (!std::getline(f, line)) continue;

        int a, b, c;
        auto dirname = d.path().filename();
        if (std::sscanf(dirname.c_str(), "%d.%d.%d", &a, &b, &c) != 3) continue;
        
        result.emplace(std::move(line), std::make_tuple(a, b, c));
    }
    
    return result;
}

std::string version_suffix(const std::string &kernel, const VersionMap &versionmap)
{
    auto it = versionmap.find(kernel);
    if (it == versionmap.end())
    {
        std::cout << WARN << "Cannot find kernel " << kernel << " in /usr/lib/modules; using made-up version number.\n";
        return "-0.0.0";
    }
    
    auto [a, b, c] = it->second;
    return "-" + std::to_string(a) + "." + std::to_string(b) + "." + std::to_string(c);
}

auto date_suffix()
{
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&now), "-%Y-%m-%d-%H-%M");
    return ss.str();
}

auto random_suffix()
{
    std::string result;
    result.reserve(9);
    
    auto gen = std::default_random_engine(std::random_device()());
    
    result.push_back('-');
    for (int i = 0; i < 8; i++)
        result.push_back('0' + std::uniform_int_distribution<int>(0, 9)(gen));
    
    return result;
}

auto generate_image_name(const std::string &kernel, const VersionMap &versionmap)
{
    auto unused = [] (const std::string &name) {return !fs::exists(Link::imagepath(name)) && !fs::exists(Link::path(name));};
    
    std::string name = kernel;

    switch (config->nameformat)
    {
        case ImageNameFormat::Date:    name += date_suffix();                      break;
        case ImageNameFormat::Version: name += version_suffix(kernel, versionmap); break;
        case ImageNameFormat::Random:  name += random_suffix();                    break;
    }
    
    if (unused(name)) return name;
    
    for (int i = 2; ; i++)
    {
        std::string name_uniq = name + "-" + std::to_string(i);
        if (unused(name_uniq)) return name_uniq;
    }
}

/*
 * Running image/subvolume and nextboot detection
 */

std::optional<std::string> extract_subvol_param(const std::string &str)
{
    std::smatch match;
    if (!std::regex_search(str, match, std::regex("(^|,|\\s|=)subvol=([^\\s,]*)"))) return {};
    return match[2].str();
}

auto resolve_script(const std::string &scriptname)
{
    auto userversion = "/etc/bootmap.d/" + scriptname;
    return fs::exists(userversion) ? std::move(userversion) : "/usr/lib/bootmap/" + scriptname;
}

std::optional<std::string> get_booted_subvol_param()
{
    std::ifstream f("/proc/cmdline");
    if (!f) return {};
    
    std::string line;
    if (!std::getline(f, line)) return {};
    
    return extract_subvol_param(line);
}

Link* get_running_image(std::vector<std::unique_ptr<Subvol>> &subvols)
{
    auto param = get_booted_subvol_param();
    
    if (param)
        for (auto &s : subvols)
            for (auto &im : s->images)
                if (im->bootpath() == *param)
                    return im.get();
    
    return nullptr;
}

Subvol* get_running_subvol(const std::string &rootopts, std::vector<std::unique_ptr<Subvol>> &subvols)
{
    auto param = extract_subvol_param(rootopts);
    
    if (param)
        for (auto &s : subvols)
            if (s->bootpath == param)
                return s.get();
    
    return nullptr;
}

Link* get_nextboot(std::vector<std::unique_ptr<Subvol>> &subvols)
{
    auto str = command_output(resolve_script("getboot.sh"));
    
    for (auto &s : subvols)
        for (auto &im : s->images)
            if (im->imagename() == str)
                return im.get();
    
    return nullptr;
}

/*
 * Finding kernels to be updated
 */

auto get_all_kernels()
{
    std::vector<std::string> kernels;
    
    std::regex regex("vmlinuz-(.*)");
    std::smatch match;
    
    for (const auto &d : fs::directory_iterator("/boot"))
        if (auto filename = d.path().filename().string(); true)
            if (std::regex_match(filename, match, regex))
                if (fs::exists("/boot/initramfs-" + match[1].str() + ".img"))
                    kernels.emplace_back(match[1].str());
    
    return kernels;
}

auto read_kernels()
{
    std::vector<std::string> kernels;
    
    std::regex kernel_regex("linux.*");
    std::regex other_regex("usr/lib/initcpio/.*|boot/.*-ucode.img");
    std::smatch match;
    
    std::string str;
    while (std::cin >> str)
    {
        if (std::regex_match(str, match, kernel_regex))
        {
            if (fs::exists("/boot/vmlinuz-" + match.str()) && fs::exists("/boot/initramfs-" + match.str() + ".img"))
                kernels.emplace_back(match.str());
        }
        else if (std::regex_match(str, other_regex))
        {
            return get_all_kernels();
        }
    }
    
    return kernels;
}

auto get_missing_kernels(const Subvol *running_subvol)
{
    auto kernels = get_all_kernels();
    
    delete_if(kernels, [&] (const std::string &kernel)
    {
        for (const auto &im : running_subvol->images)
            if (im->matches_kernel(kernel))
                return true;
            
        return false;
    });
    
    return kernels;
}

/*
 * Reading contents of BootmapVol
 */

using Extraneous = std::pair<fs::path, std::string>;

std::string get_backup_stem()
{
    std::string escaped;
    
    for (auto ch : config->rootvol | std::views::drop(1))
             if (ch == '\\') escaped += "\\\\";
        else if (ch == '/' ) escaped += "\\-";
        else                 escaped += ch;
    
    return escaped + "-backup-";
}

auto load_bootmap()
{
    std::vector<std::unique_ptr<Link>> links;
    std::vector<std::unique_ptr<Subvol>> subvols;
    std::vector<Extraneous> extraneous;
    
    subvols.emplace_back(std::make_unique<Subvol>("INACCESSIBLE", config->rootvol));
    
    // Read contents of bootmap directory
    for (const auto &d : fs::directory_iterator(config->bootmapdir))
        if (d.is_symlink())
            links.emplace_back(std::make_unique<Link>(d.path()));
        else if (d.is_directory() && is_btrfs_subvolume(d.path()))
            subvols.emplace_back(std::make_unique<Subvol>(d.path()));
        else
            extraneous.emplace_back(d.path(), "Not a symlink or a subvolume");
    
    // Assign Links to Subvols
    for (auto &l : links)
    {
        if (!fs::exists(l->imagepath()))
        {
            // The efi image is missing: ignore the Link
            extraneous.emplace_back(l->path(), "Symlink is missing corresponding EFI image " + l->imagepath());
            continue;
        }
        
        for (auto &s : subvols)
        {
            if (l->target == s->bootpath)
            {
                // Found the corresponding Subvol: add the Link to it
                s->images.emplace_back(std::move(l));
                goto nested_continue;
            }
        }
        
        // Could not find the corresponding Subvol: ignore the Link
        extraneous.emplace_back(l->path(), "Symlink whose destination " + l->target + " is not one of " + comma_separated(subvols | std::views::transform([] (decltype(subvols)::const_reference s) -> auto& {return s->bootpath;})));
        extraneous.emplace_back(l->imagepath(), "EFI image associated with the above");
        
        nested_continue:;
    }
    
    // Remove Subvols with no Links, with the exception of the root subvolume which should always be present
    delete_if(subvols, [&] (decltype(subvols)::const_reference s)
    {
        if (s.get() == subvols[0].get()) return false;
        if (s->images.empty()) {extraneous.emplace_back(s->path, "Unused subvolume"); return true;}
        return false;
    });
    
    // Determine backup number of each Subvolume
    auto backup_prefix = config->bootmapvol + "/" + get_backup_stem();
    
    subvols[0]->num = 0;    
    
    for (auto &s : subvols | std::views::drop(1))
    {
        s->num = Subvol::NonBackup;
        
        if (s->bootpath.starts_with(backup_prefix))
            try
            {
                int num = std::stoi(s->bootpath.substr(backup_prefix.length()));
                if (num >= 1) s->num = num;
            }
            catch (const std::invalid_argument&) {}
    }
    
    return std::make_pair(std::move(subvols), std::move(extraneous));
}

void sort_subvols(std::vector<std::unique_ptr<Subvol>> &subvols, Link *running_image, Subvol *running_subvol, Link *nextboot)
{
    // Sort Subvols by backup number, then by running, then by alphabetical order
    std::sort(subvols.begin(), subvols.end(), [=] (const std::unique_ptr<Subvol> &a, const std::unique_ptr<Subvol> &b)
    {
        if (a->num < b->num) return true;
        if (a->num > b->num) return false;
        if (a.get() == running_subvol) return true;
        if (b.get() == running_subvol) return false;
        return a->bootpath < b->bootpath;
    });
    
    // Sort images by running, then by nextboot, then by alphabetical order
    for (auto &s : subvols)
        std::sort(s->images.begin(), s->images.end(), [=] (decltype(s->images)::const_reference a, decltype(s->images)::const_reference b)
        {
            if (a.get() == running_image) return true;
            if (b.get() == running_image) return false;
            if (a.get() == nextboot) return true;
            if (b.get() == nextboot) return false;
            return a->name < b->name;
        });
}

int main(int argc, char **argv) try
{
    // Parse commandline
    auto command = get_command(argc, argv);
    
    // Deal with Help command
    if (command == Command::Help)
    {
        std::cout << help_msg;
        return 0;
    }
    
    // Require root for all commands other than the Status command
    if (command != Command::Status) ensure_root();
    
    // Load config and make available as global variable
    auto config = load_config();
    ::config = &config;
    
    // Create bootmapdir if missing
    auto bootmaptmpdir = fs::exists(config.bootmapdir) ? std::unique_ptr<TmpDir>() : (ensure_root(), std::make_unique<TmpDir>(config.bootmapdir));
    
    // Trigger automounting of BootmapVol and ESP
    ensure_automounted(config.esp);
    ensure_automounted(config.bootmapdir);
    
    // Get mounts table and fstab
    auto mounts = get_mountinfo();
    auto fstab = get_fstab();
    
    auto find_mount = [&] (const std::string &path) {return mnt_table_find_target(mounts.get(), path.c_str(), MNT_ITER_BACKWARD);};
    auto find_fstab = [&] (const std::string &path) {return mnt_table_find_target(fstab.get(),  path.c_str(), MNT_ITER_BACKWARD);};
    auto is_user_mountable = [&] (const std::string &path) {auto fs = find_fstab(path); if (!fs) return false; auto opts = mnt_fs_get_options(fs); return mnt_optstr_get_option(opts, "user", nullptr, nullptr) == 0 || mnt_optstr_get_option(opts, "users", nullptr, nullptr) == 0;};
    
    // Get mount information for system root
    auto [rootdev, rootopts] = [&] {auto rootinfo = get_root_mount(mounts.get()); return std::make_pair(std::string(mnt_fs_get_srcpath(rootinfo)), std::string(mnt_fs_get_options(rootinfo)));}();
    
    // Mount BootmapVol and ESP if necessary
    bool refresh_mounts = false;
    
    auto bootmaptmpmount = find_mount(config.bootmapdir)
                         ? std::unique_ptr<TmpMount>()
                         : geteuid() == 0
                         ? (refresh_mounts = true, std::make_unique<TmpMount>(rootdev, config.bootmapvol, config.bootmapdir))
                         : is_user_mountable(config.bootmapdir)
                         ? (refresh_mounts = true, std::make_unique<TmpMount>(config.bootmapdir))
                         : throw std::runtime_error("Need root to mount BootmapVol");
    
    auto esptmpmount = find_mount(config.esp)
                     ? std::unique_ptr<TmpMount>()
                     : find_fstab(config.esp)
                     ? (refresh_mounts = true, std::make_unique<TmpMount>(config.esp))
                     : throw std::runtime_error("EFI system partition not mounted and not in fstab");
    
    if (refresh_mounts) mounts = get_mountinfo();
    
    // Read contents of BootmapVol
    auto [subvols, extraneous] = load_bootmap();
    
    // For temporarily mounting filesystems as read-write if they aren't already
    auto ensure_readwrite = [&] (const std::string &path)
    {
        auto fs = find_mount(path);
        if (!fs) throw std::runtime_error(path + " is not a mount point");
        
        auto opts = mnt_fs_get_options(fs);
        auto is_ro = mnt_optstr_get_option(opts, "ro", nullptr, nullptr) == 0;
        auto is_subvol = (bool)extract_subvol_param(opts);
        
        return is_ro ? std::make_unique<TmpRemountRW>(path, is_subvol) : std::unique_ptr<TmpRemountRW>();
    };
    
    switch (command)
    {
        case Command::Status:
        {
            if (!extraneous.empty())
            {
                std::cerr << WARN << "The following extraneous files were detected. Please run cleanup to delete them.\n";
                
                Table table;
                table.add_row("FILE", "REASON");
                for (const auto &e : extraneous)
                    table.add_row(e.first, e.second);
                table.print(std::cerr);
            }
            
            auto running_image = get_running_image(subvols);
            if (!running_image) std::cerr << WARN << "The booted kernel does not correspond to any known image.\n";
            
            auto running_subvol = get_running_subvol(rootopts, subvols);
            if (!running_subvol) std::cerr << WARN << "The system root does not correspond to any known subvolume.\n";
            
            auto nextboot = get_nextboot(subvols);
            if (!nextboot) std::cerr << WARN << "No known image is set as next boot.\n";

            sort_subvols(subvols, running_image, running_subvol, nextboot);
            
            Table table;
            table.add_row("IMAGE", "", "SUBVOLUME", "USED AS");
            for (const auto &s : subvols)
            {
                std::string sname = s->bootpath;
                
                if (s.get() == running_subvol)
                    sname += GRN + std::string(" [running]") + NRM;
                
                std::string usedas;
                
                if (s->num == 0)
                    usedas = "Current";
                else if (s->num != Subvol::NonBackup)
                    usedas = "Backup " + std::to_string(s->num);
                else
                    usedas = "";
                
                if (s->images.empty())
                {
                    table.add_row("", "", sname, usedas);
                    continue;
                }
                
                for (auto &im : s->images)
                {
                    std::string iname = im->name;
                    
                    if (im.get() == running_image)
                        iname += GRN + std::string(" [running]") + NRM;
                    else if (im.get() == nextboot)
                        iname += GRN + std::string(" [nextboot]") + NRM;
                    
                    if (&im == &s->images.front())
                        table.add_row(iname, "→", sname, usedas);
                    else
                        table.add_row(iname, "↗", "", "");
                }
            }
            table.print(std::cout);
            
            break;
        }
        
        case Command::Cleanup:
        {
            if (extraneous.empty())
            {
                std::cout << "Nothing to do\n";
                break;
            }
            
            auto esp_remount = ensure_readwrite(config.esp);
            auto bootmap_remount = ensure_readwrite(config.bootmapdir);
            
            for (const auto &e : extraneous)
                if (is_btrfs_subvolume(e.first))
                {
                    delete_btrfs_subvolume(e.first);
                }
                else
                {
                    std::cout << "Deleting " << e.first<< '\n';
                    fs::remove_all(e.first);
                }
            
            break;
        }
        
        case Command::Pre:
        {
            auto running_image = get_running_image(subvols);
            
            auto running_subvol = get_running_subvol(rootopts, subvols);
            if (!running_subvol) throw std::runtime_error("The system root does not correspond to any known subvolume. No action will be taken.");
            
            auto image_needs_removal = [kernels = read_kernels()] (const Link &im)
            {
                for (const auto &k : kernels)
                    if (im.matches_kernel(k))
                        return true;
                    
                return false;
            };
            
            auto esp_remount = ensure_readwrite(config.esp);
            auto bootmap_remount = ensure_readwrite(config.bootmapdir);
            
            // Rotate backups if necessary
            if (running_subvol->num == 0 && running_image && running_image->target == running_subvol->bootpath && image_needs_removal(*running_image))
            {
                // Ensure the first subvolumes in the list are the backups, in order
                sort_subvols(subvols, nullptr, nullptr, nullptr);
                
                // Ignore non-backups from now on
                while (subvols.back()->num == Subvol::NonBackup)
                    subvols.pop_back();
                
                // Delete unnecessary backups to bring the number below MaxBackups, if possible
                while (subvols.size() >= 2 && subvols.size() > config.maxbackups)
                {
                    std::cout << "Deleting old backup " << subvols.back()->bootpath << '\n';
                    subvols.back()->remove_all();
                    subvols.pop_back();
                }
                
                // The rest does not need doing if no backups are to be kept
                if (config.maxbackups == 0) goto break_if;
                
                // Rotate existing backups to be numbered 2,3,4,...
                auto backup_stem = get_backup_stem();
                BatchRenamer renamer;
                
                for (int i = 1; i < subvols.size(); i++)
                {
                    auto &s = *subvols[i];
                 
                    // Renumber
                    s.num = i + 1;
                    
                    // Rename paths (to be committed to filesystem later)
                    renamer.rename(s.path,      config.bootmapdir + "/" + backup_stem + std::to_string(s.num));
                                   s.bootpath = config.bootmapvol + "/" + backup_stem + std::to_string(s.num);
                    
                    // Update links
                    for (auto &im : s.images)
                        im->update_target(s.bootpath);
                }
                
                renamer.commit();
                
                // Create snapshot of system root
                auto backup = std::make_unique<Subvol>(config.bootmapdir + "/" + backup_stem + "1");
                run_command("btrfs", "subvolume", "snapshot", "/", backup->path);
                
                // Move running image to backup
                running_image->update_target(backup->bootpath);
                
                if (auto &images = subvols[0]->images; true)
                    for (int i = 0; i < images.size(); i++)
                        if (images[i].get() == running_image)
                        {
                            backup->images.emplace_back(std::move(images[i]));
                            images.erase(images.begin() + i);
                            break;
                        }
                
                // Make it the first backup
                backup->num = 1;
                subvols.emplace(subvols.begin() + 1, std::move(backup));
            }
            else
            {
                // No preparation necessary, but warn if modifying a backup
                if (running_subvol->num != 0 && running_subvol->num != Subvol::NonBackup)
                    std::cerr << WARN << "You are currently modifying a BACKUP!\n";
            }
            break_if:;
            
            // Delete images
            delete_if(running_subvol->images, [&] (const std::unique_ptr<Link> &im)
            {
                if (image_needs_removal(*im)) {std::cout << "Deleting image " << im->name << '\n'; im->remove_all(); return true;}
                return false;
            });
            
            break;
        }
        
        case Command::Post:
        case Command::Install:
        {
            auto running_subvol = get_running_subvol(rootopts, subvols);
            if (!running_subvol) throw std::runtime_error("The system root does not correspond to any known subvolume. No action will be taken.");
            
            std::vector<std::string> kernels;
            if (command == Command::Post)
            {
                kernels = read_kernels();
            }
            else // command == Command::Install
            {
                kernels = get_missing_kernels(running_subvol);
            }
            
            if (kernels.empty())
            {
                std::cout << "Nothing to do\n";
                break;
            }
            
            auto versionmap = load_versionmap();
            
            auto esp_remount = ensure_readwrite(config.esp);
            auto bootmap_remount = ensure_readwrite(config.bootmapdir);
            
            for (const auto &kernel : kernels)
            {
                // Create the image in memory
                auto im = std::make_unique<Link>(generate_image_name(kernel, versionmap), running_subvol->bootpath);
                
                // Create the image on the filesystem
                std::cout << "Generating image " << im->imagepath() << '\n';
                run_command(resolve_script("generate.sh"), kernel, im->bootpath(), im->imagepath());
                
                // Check it was created, and error out if not
                if (!fs::exists(im->imagepath()))
                {
                    std::cerr << ERR << "Failed to create image for " << kernel << '\n';;
                    continue;
                }
                
                // Create the symlink too
                im->create_link();
                
                // Set as next boot if asked
                if (im->matches_kernel(config.bootnext))
                {
                    std::cout << "Setting next boot to " << im->imagename() << '\n';
                    run_command(resolve_script("setboot.sh"), im->imagename());
                }
                
                // Add to the list of images
                running_subvol->images.emplace_back(std::move(im));
            }
            
            break;
        }
        
        case Command::Help:
        {
            // Handled earlier
            assert(false);
            break;
        }
    }
    
    return 0;
}
catch (const std::runtime_error &e)
{
    std::cerr << ERR << e.what() << '\n';
    return 1;
}
