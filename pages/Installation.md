# Installation

## Prerequisites

To use bootmap your system must meet some basic requirements:

 * The EFI system partition must be located at one of `/efi`, `/boot/efi`, or `/boot`.
 
   Bootmap is very flexible about mounting. As long as there's an fstab entry for the EFI system partition, it's fine, whether actually mounted or not. You can use any combination of noauto, user/users, x-systemd.automount, and ro. But try to avoid mounting it at `/boot`.
   
 * The root filesystem must be btrfs.
 
   This is required to use snapshots. Patches are welcome for other supportable filesystems!

By default, bootmap also makes the following common assumptions about your system. If not, you can still use bootmap, but will need to edit the default configuration.

 * The root subvolume is named `/@`.
   
   This location is recommended by the [Arch  Wiki](https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout) if you are using snapper. If not, edit the `RootVol` config option.

 * You have [systemd-boot](https://wiki.archlinux.org/title/Systemd-boot) installed to the EFI partition.

   If you want to use a different bootloader, or none at all, you can edit the `ImageDir` option, or copy the `setboot.sh`/`getboot.sh` scripts to /etc/bootmap.d and edit them.

 * You are using the `linux` kernel.
 
   To use other kernels, such as `linux-zen`, edit the `BootNext` option.

## Installation

The installation instructions are then as follows:

 * Install the `bootmap` package [from AUR](https://aur.archlinux.org/packages/bootmap).
 
 * Create a subvolume called `/bootmap` relative to the btrfs filesystem root. For example, as root, run

       mount MY-FILESYSTEM /mnt
       btrfs subvolume create /mnt/bootmap
       umount /mnt

   If you like, you can use a different name for this subvolume, provided you edit the `BootmapVol` option.

 * Edit any desired configration files now. For example, look through `/etc/bootmap.conf` and ensure the defaults make sense. Make sure the default `/etc/kernel/cmdline` makes sense, or replace it with your own. If you do, include `subvol=$SUBVOL` in the root mount options, which bootmap will replace at image generation time.
 
 * If you want your images to be signed, make sure that your Secure Boot keys are in `/etc/efi-keys/`. This is the location recommended by the [Arch Wiki](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Creating_keys). If there are no keys here, images will not be signed. You can always come back and do this step later. But make sure you do so before turning on Secure Boot! To check if if an image has been signed, use `sbverify`, for example like this:
   
         sbverify --cert /etc/efi-keys/db.crt $IMAGE.efi
   
   If you want to change something else about how signing works, copy the `generate.sh` script to /etc/bootmap.d and edit it.
   
   In keeping with the Arch Way, bootmap does not attempt to generate Secure Boot keys for you automatically. For instructions on how to do this, check the link above or [Rod's Books](https://rodsbooks.com/efi-bootloaders/controlling-sb.html).
 
 * Run `bootmap install`. This will generate an image for each kernel on the EFI partition, and create accompanying symlinks in `/bootmap`.

That's it!

## Operation

Upon kernel updates, bootmap will now create/delete signed images on the EFI partition, create/delete accompanying symlinks and snapshots in `/bootmap`, and edit the systemd-boot configuration file to set the default image to boot. You can boot any of these backup images manually, or through your bootloader. Otherwise the system will boot into the most up-to-date version of the kernel set in the config file.

*How it works:*  
For such a critical system component, it's important to know exactly what's going on. Here's an explanation. When bootmap creates an EFI image `$NAME.efi`, it creates an identically-named symlink `/bootmap/$NAME`. It bakes into the image an instruction to always boot to this symlink (by replacing the $SUBVOL variable in the kernel commandline). You can check this by inspecting the kernel commandline of an image, using

    objdump $IMAGE.efi -sj .cmdline

and the symlink will appear as the `subvol` mount option for the root device.

Each symlink points either to the root subvolume `/@` or to a backup snapshot. When the backups need to be rotated, these symlinks are rewritten. This allows the subvolumes booted to by each image to be updated without touching the images.

Note also that bootmap does not maintain any external state. Its state is encoded purely in the collection of symlinks in `/bootmap`. This means that you can learn the same information that the `status` command tells you by running `ls -l` in this directory, assuming it is mounted. It also means that you can easily make manual edits. All of the following edits will result in no state becoming inconsistent:

* Create/delete/rename subvolumes (provided you update the symlinks that point to them).
* Create/delete symlinks (provided you do the same for the corresponding EFI images).
* Rename/move the root subvolume (provided you rename all its backups and update `RootVol` in the config file).

However, please refrain from renaming images (and therefore symlinks). Otherwise, the commandline baked into the image will go out of sync with its displayed name, and then things will just get confusing! If you do break the rules above, the `status` command will tell you about it, and the `cleanup` command will sort it out (by deleting the problem).

## Post-installation

The installation process was designed to be as uninvasive and configurationless as possible. There are however a number of enhancements you can perform after installation:

 * You may notice that you can only run `bootmap status` as root. This is undesirable, but necessary because bootmap needs to create `/run/bootmap` and mount `/bootmap` there and this requires root. To fix this, you can first create `/run/bootmap` by adding an entry in `/etc/tmpfiles.d`, then add an entry in `/etc/fstab` to ensure `/bootmap` is either always mounted, or user-mountable.
 
 * If you really want to be able to sleep at night, you can mount both `/bootmap` and the EFI system partition read-only, or not at all. This will protect them against an accidental `rm -rf`. If you do this, then bootmap will temporarily mount them read-write when it needs to make modifications.

 * The default image naming scheme uses the current timestamp. This is a sensible choice if you are going to be repeatedly uninstalling and reinstalling kernels that go by the same version number. This might be the case if you are hunting down a kernel bug, for example, or stress-testing this software. But for aesthetic reasons, you may prefer images to be named using the kernel version (as in the screenshot). To enable this, set `NameFormat=date`. The only downside is that the potential lack of image name uniqueness will result in unnecessary backup snapshots being made if you reinstall the kernel more than once without rebooting.

 * You may also want to raise the `MaxBackups` option from its default value of 1. Setting it to 2 would protect you if you broke the `sudoers` file, ran a kernel update without noticing, then rebooted. If you are a Neville Longbottom you might wish to consider even higher values. But make sure you don't run out of space on your EFI partition!
