INSTALL ?= install

.PHONY: all install

all:
	$(MAKE) -C src
	$(MAKE) -C doc

install:
	$(INSTALL) -Dm755 -t "$(DESTDIR)/usr/bin" src/bootmap
	$(INSTALL) -Dm644 -t "$(DESTDIR)/etc" conf/bootmap.conf
	$(INSTALL) -Dm644 -t "$(DESTDIR)/etc/kernel" conf/cmdline
	$(INSTALL) -Dm755 -t "$(DESTDIR)/usr/lib/bootmap" scripts/*
	$(INSTALL) -Dm644 -t "$(DESTDIR)/usr/share/libalpm/hooks" hooks/*
	$(INSTALL) -Dm644 -t "$(DESTDIR)/usr/share/man/man8" doc/bootmap.8
	$(INSTALL) -Dm644 -t "$(DESTDIR)/usr/share/bash-completion/completions" bash-completion/bootmap
