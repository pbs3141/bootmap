#! /bin/bash
#
# getboot.sh - Called by bootmap to determine the image set as next boot.
#

if [ -d /efi ]; then
    ESP=/efi
elif [ -d /boot/efi ]; then
    ESP=/boot/efi
elif [ -d /boot ]; then
    ESP=/boot
else
    echo "$0: Failed to find EFI system partition" >&2
    exit 1
fi

perl -n -e 'print $1 if /^default\s(.*)/' "$ESP"/loader/loader.conf # Return value e.g. linux-123.efi
