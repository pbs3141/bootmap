#! /bin/bash
#
# generate.sh - Called by bootmap to generate, sign and install an EFI image.
#

kernel="$1" # Name of the kernel                   e.g. linux
subvol="$2" # Subvolume the image should boot into e.g. /bootmap/linux-123
output="$3" # Where the image should be placed     e.g. $esp/EFI/Linux/linux-123.efi

shopt -s extglob nullglob

rootdev=/dev/disk/by-uuid/"$(findmnt / -no UUID)" || exit
cmdline="$(ROOTDEV="$rootdev" SUBVOL="$subvol" perl -p -e 's/\$ROOTDEV/$ENV{ROOTDEV}/g; s/\$SUBVOL/$ENV{SUBVOL}/g; chomp if eof' /etc/kernel/cmdline)" || exit

args=()
args+=(--linux=/boot/vmlinuz-"$kernel")
args+=(--initrd=/boot/*-ucode.img)
args+=(--initrd=/boot/initramfs-"$kernel".img)
args+=(--cmdline="$cmdline")
args+=(--os-release=@/usr/lib/os-release)
args+=(--splash=/usr/share/systemd/bootctl/splash-arch.bmp)
args+=(--output="$output")

keys=(/etc/efi-keys/@(db|DB).key); key="${keys[0]}"
crts=(/etc/efi-keys/@(db|DB).crt); crt="${crts[0]}"
if [ -n "$key" ] && [ -n "$crt" ]; then
    args+=(--signtool=sbsign)
    args+=(--secureboot-private-key="$key")
    args+=(--secureboot-certificate="$crt")
else
    echo "Generating unsigned image"
fi

/usr/lib/systemd/ukify build "${args[@]}"
