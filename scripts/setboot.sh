#! /bin/bash
#
# setboot.sh - Called by bootmap to set an image as the next boot.
#

image="$1" # The image name e.g. linux-123.efi

if [ -d /efi ]; then
    ESP=/efi
elif [ -d /boot/efi ]; then
    ESP=/boot/efi
elif [ -d /boot ]; then
    ESP=/boot
else
    echo "$0: Failed to find EFI system partition" >&2
    exit 1
fi

sed -e '/^default\b/d' -e '$a\' -i "$ESP"/loader/loader.conf
echo "default $image" >> "$ESP"/loader/loader.conf
